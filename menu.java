// WARNING: This file is auto-generated and any changes to it will be overwritten
import lang.stride.*;
import java.util.*;
import greenfoot.*;

/**
 * 
 */
public class menu extends World
{

    /**
     * Constructor for objects of class menu.
     */
    public menu()
    {
        super(600, 400, 1);
        prepare();
    }

    /**
     * Prepare the world for the start of the program.
     * That is: create the initial objects and add them to the world.
     */
    private void prepare()
    {
        main main =  new  main();
        addObject(main, 440, 132);
        game game =  new  game();
        addObject(game, 230, 75);
    }
}
