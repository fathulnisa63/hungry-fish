// WARNING: This file is auto-generated and any changes to it will be overwritten
import lang.stride.*;
import java.util.*;
import greenfoot.*;

/**
 * 
 */
public class laut extends MyWorld
{

    /**
     * Constructor for objects of class laut.
     */
    public laut()
    {
        prepare();
    }

    /**
     * Prepare the world for the start of the program.
     * That is: create the initial objects and add them to the world.
     */
    private void prepare()
    {
        ikan1 ikan1 =  new  ikan1();
        addObject(ikan1, 68, 155);
        ikan2 ikan2 =  new  ikan2();
        addObject(ikan2, 544, 98);
        ikan1.setLocation(60, 167);
        telur1 telur1 =  new  telur1();
        addObject(telur1, 182, 278);
        kepiting kepiting =  new  kepiting();
        addObject(kepiting, 33, 373);
        bom bom =  new  bom();
        addObject(bom, 543, 321);
        telur1 telur12 =  new  telur1();
        addObject(telur12, 568, 221);
        telur1 telur13 =  new  telur1();
        addObject(telur13, 394, 320);
        telur1 telur14 =  new  telur1();
        addObject(telur14, 38, 277);
        bom bom2 =  new  bom();
        addObject(bom2, 329, 263);
        bom2.setLocation(242, 336);
        bom bom3 =  new  bom();
        addObject(bom3, 375, 252);
    }
}
